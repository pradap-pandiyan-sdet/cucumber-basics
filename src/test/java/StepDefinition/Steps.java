package StepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Steps {

    WebDriver driver;

//Launching Chrome Browser



    @Given("^Open the Chrome and launch the application$")
    public void open_the_Chrome_and_launch_the_application() throws Throwable {
        System.setProperty("webdriver.chrome.driver","F:\\jar\\chromedriver.exe");
        driver= new ChromeDriver();
        driver.navigate().to("https://stackoverflow.com");
        driver.manage().timeouts().pageLoadTimeout(10,TimeUnit.SECONDS);


        System.out.println("This Step open the Firefox and launch the application.");
    }
    //Once Chrome browser is launched check the current title
    @When("^Get the current URL$")
    public void get_the_current_url() throws Throwable
    {

         System.out.println("Current Title: "+driver.getTitle());

     }

    //Once the title matches click on Login
    @Then("^Click on Login$")
    public void click_on_login() throws Throwable
    {
        if(driver.getTitle().contains("Stack Overflow"))
        {
            driver.findElement(By.xpath("//a[@class='login-link s-btn btn-topbar-clear py8']")).click();
            System.out.println("Clicking Login button");
        }

    }

}